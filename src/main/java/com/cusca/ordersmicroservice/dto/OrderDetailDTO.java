package com.cusca.ordersmicroservice.dto;


import lombok.Data;

@Data
public class OrderDetailDTO {


    private Long id;

    private int idOrder;


    private int idProduct;


    private  String price;


    private String title;

    private int quantity;
}
