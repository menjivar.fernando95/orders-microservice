package com.cusca.ordersmicroservice.dto;

import com.cusca.ordersmicroservice.entity.OrderDetail;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.OneToMany;


import java.util.Date;
import java.util.List;

@Data
public class OrderDTO {

    private Long id;
    private int idClient;

    private String sendAddress;

    private List<OrderDetail> orderDetail;
}
