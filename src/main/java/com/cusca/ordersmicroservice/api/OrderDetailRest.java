package com.cusca.ordersmicroservice.api;

import com.cusca.ordersmicroservice.dto.OrderDetailDTO;
import com.cusca.ordersmicroservice.entity.Order;
import com.cusca.ordersmicroservice.entity.OrderDetail;
import com.cusca.ordersmicroservice.services.OrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(path = "api/order/detail")
public class OrderDetailRest {

    @Autowired
    private OrderDetailService os;


    @GetMapping
    public ResponseEntity<List<OrderDetail>> getAllProducts(){
        try{
            return new ResponseEntity<>(this.os.findAllOrders(), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/{id}")
    public ResponseEntity<OrderDetail> getOrderProduct(@PathVariable("id") Long id){
        try{
            Optional<OrderDetail> order = this.os.findOrderById(id);
            if(order.isPresent()){
                return new ResponseEntity<>(order.get(), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>( HttpStatus.NOT_FOUND);
            }

        }catch(Exception e){
            return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping
    public ResponseEntity<Boolean> save(@RequestBody List<OrderDetailDTO> order){
        try{
            List<OrderDetail> orders = new ArrayList<>();
            for(OrderDetailDTO o : order){
                Order or= new Order();
                OrderDetail orderObject = new OrderDetail();
                orderObject.setId(o.getId());
                orderObject.setTitle(o.getTitle());
                orderObject.setIdProduct(o.getIdProduct());
                orderObject.setQuantity(o.getQuantity());
                orderObject.setPrice(o.getPrice());
                or.setId((long) o.getIdOrder());
                orderObject.setOrder(or);
                orders.add(orderObject);
            }
            return new ResponseEntity<>( this.os.saveOrUpdate(orders), HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteProduct(@PathVariable("id") Long id){
        try{
            this.os.deleteOrderDetail(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
