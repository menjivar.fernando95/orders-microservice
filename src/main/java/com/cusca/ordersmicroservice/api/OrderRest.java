package com.cusca.ordersmicroservice.api;

import com.cusca.ordersmicroservice.dto.OrderDTO;
import com.cusca.ordersmicroservice.entity.Order;
import com.cusca.ordersmicroservice.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(path = "/api/order")
public class OrderRest {


    @Autowired
    private OrderService os;


    @GetMapping
    public ResponseEntity<List<Order>> getAllOrders(){
        try{
            return new ResponseEntity<>(this.os.findAllOrders(), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/{id}")
    public ResponseEntity<Order> getOrderProduct(@PathVariable("id") Long id){
        try{
            Optional<Order> order = this.os.findOrderById(id);
            if(order.isPresent()){
                return new ResponseEntity<>(order.get(), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>( HttpStatus.NOT_FOUND);
            }

        }catch(Exception e){
            return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PostMapping
    public ResponseEntity<Order> save(@RequestBody OrderDTO o){
        try{

            Order order = new Order();
            order.setId(o.getId());
            order.setIdClient(o.getIdClient());
            order.setSendAddress(o.getSendAddress());
            order.setOrderDetail(o.getOrderDetail());

            return new ResponseEntity<>( this.os.saveOrUpdate(order), HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteProduct(@PathVariable("id") Long id){
        try{
            this.os.deleteOrder(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
