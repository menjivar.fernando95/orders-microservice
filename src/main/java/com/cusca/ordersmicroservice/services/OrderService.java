package com.cusca.ordersmicroservice.services;

import com.cusca.ordersmicroservice.entity.Order;
import com.cusca.ordersmicroservice.repository.IOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    @Autowired
    private IOrderRepository ior;

    public List<Order> findAllOrders(){
        List<Order> order = new ArrayList<>();
        this.ior.findAll().stream().forEach(order::add);
        return order;
    }

    public Order saveOrUpdate(Order order) {
    return this.ior.save(order);
    }

    public Optional<Order> findOrderById(Long id){
        return this.ior.findById(id);
    }

    public boolean deleteOrder(Long id){
        this.ior.deleteById(id);
        return true;
    }

}
