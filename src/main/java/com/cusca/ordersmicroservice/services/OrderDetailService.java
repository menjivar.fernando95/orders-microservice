package com.cusca.ordersmicroservice.services;

import com.cusca.ordersmicroservice.entity.OrderDetail;
import com.cusca.ordersmicroservice.repository.IOrderDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderDetailService {

    @Autowired
    private IOrderDetailRepository ior;

    public List<OrderDetail> findAllOrders(){
        List<OrderDetail> products = new ArrayList<>();
        this.ior.findAll().stream().forEach(products::add);
        return products;
    }

    public Boolean saveOrUpdate(List<OrderDetail> order){
        this.ior.saveAll(order);
        return true;
    }

    public Optional<OrderDetail> findOrderById(Long id){
        return this.ior.findById(id);
    }

    public boolean deleteOrderDetail(Long id){
        this.ior.deleteById(id);
        return true;
    }

}
