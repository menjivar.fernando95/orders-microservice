package com.cusca.ordersmicroservice.repository;

import com.cusca.ordersmicroservice.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IOrderRepository extends JpaRepository<Order, Long> {

    @Query(value = "select o from Order o where o.idClient = ?1")
    List<Order> findByIdClient(int id);
}
