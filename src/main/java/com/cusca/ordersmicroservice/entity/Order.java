package com.cusca.ordersmicroservice.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "order", schema = "public")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "id_client")
    private int idClient;

    @Column(name = "sendAddress")
    private String sendAddress;

    @Column(name = "date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "order", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<OrderDetail> orderDetail;
}
