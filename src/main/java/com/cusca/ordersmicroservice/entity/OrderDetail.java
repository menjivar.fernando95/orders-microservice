package com.cusca.ordersmicroservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "order_detail", schema = "public")
public class OrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "id_product")
    private int idProduct;

    @Column(name = "title")
    private String title;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "price")
    private  String price;

    @ManyToOne(optional = false, fetch = FetchType.LAZY )
    @JoinColumn(name = "id_order", nullable = false, updatable = false)
    @JsonIgnore
    private Order order;



}